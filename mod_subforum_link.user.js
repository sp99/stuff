// ==UserScript==
// @name         Mod Subforum Category Link
// @namespace    *.oneplus.net*
// @version      0.1
// @description  Category link to mod subforum ;) SIDEBAR REQUIRED!
// @author       Sam Prescott (sp99)
// @include      *forums.oneplus.net*
// @grant        none
// @license      MIT License; http://opensource.org/licenses/MIT
// ==/UserScript==

$(document).ready(function () {
	if (typeof sidebarVersion !== 'undefined') {
		$("<li><a href=\"/forums/moderation_team/\">Mod Subforum</a></li>").insertAfter($("#widget-11 ul.xenforo-list-2cols li:eq(10)"));
	}
	else {
		setTimeout(function(){
    				if (typeof sidebarVersion !== 'undefined') {
					$("<li><a href=\"/forums/moderation_team/\">Mod Subforum</a></li>").insertAfter($("#widget-11 ul.xenforo-list-2cols li:eq(10)"));
				}
				else {
					alert("Sidebar Required!");
				}
  		}, 5000);
	}
});